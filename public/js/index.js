const ratio = 0.1;

const options = {
    root: null,
    rootMargin: '0px',
    threshold: ratio
};

const handlerIntersect = (entries, observer) => {

    entries.forEach((entry) => {

        if (entry.intersectionRatio > ratio) {

            entry.target.classList.remove('reveal');

            observer.unobserve(entry.target);

        }

    });
};
  
const observer = new IntersectionObserver(handlerIntersect, options);

document.querySelectorAll('.reveal').forEach((element) => {
    observer.observe(element);
});

const navbar = document.getElementById('navbar');

window.addEventListener('scroll', () => {
    navbar.classList.toggle('sticky', window.scrollY > 0);
});

//'js-link'
document.addEventListener('click', (event) => {

    if (isClickOnButtonMenu(event)) {
        toggleMenuMobile();
        toggleBodyOverflow();
        return;
    }

    if (isClickOnButtonChangeLang(event)) {
        addBodyOverflow();
        openLanguagesList();
        return;
    }

    if (isClickOnLang(event)) {
        removeBodyOverflow();
        closeLanguagesList();
        const newLang = updateNavLang(event);
        postNewLang(newLang);
        return;
    }

    if (isClickOnCloseLanguageModal(event)) {
        removeBodyOverflow();
        closeLanguagesList();
        return;
    }

    if (!isClickOnMenuMobile(event) && isMenuMobileOpen()) {
        toggleMenuMobile();
        toggleBodyOverflow();
        return;
    }
});

function isClickOnButtonMenu(event) {
    const menuMobileButton = document.getElementById('menu-mobile-button');

    if (menuMobileButton.contains(event.target)) {
        return true;
    }

    return false;

}

function isClickOnMenuMobile(event) {
    const menuMobile = document.getElementById('menu-mobile');

    if (!menuMobile.contains(event.target)) {
        return false;
    }

    if (event.target.classList.contains('js-link')) {
        return false;
    }


    return true;
}

function isMenuMobileOpen() {
    const navbar = document.getElementById('navbar');

    if (navbar.classList.contains('active-menu-mobile')) {
        return true;
    }

    return false;
}

function toggleMenuMobile() {
    const navbar = document.getElementById('navbar');

    if (navbar.classList.contains('active-menu-mobile')) {
        navbar.classList.remove('active-menu-mobile');
        return;
    }

    navbar.classList.add('active-menu-mobile');
    return;
}

function toggleBodyOverflow() {
    const body = document.body;

    if (body.classList.contains('menu-mobile-open')) {
        body.classList.remove('menu-mobile-open');
        const main = document.getElementById('modal-main-content');

        removeScrollPositionFromElement(main);

        document.documentElement.classList.remove('menu-mobile-open');
        return;
    }

    const main = document.getElementById('modal-main-content');

    fitElementToScrollPosition(main);

    body.classList.add('menu-mobile-open');

    document.documentElement.classList.add('menu-mobile-open');
    return;

}

function isClickOnButtonChangeLang(event) {
    const buttonChangeLang = document.getElementById('nav-button-lang');
    const mobileChangeLang = document.getElementById('mobile-button-lang');

    if (buttonChangeLang.contains(event.target)) {
        return true;
    }

    if (mobileChangeLang.contains(event.target)) {
        return true;
    }

    return false;
}

function isClickOnCloseLanguageModal(event) {
    const buttonCloseLanguages = document.getElementById('languages-close');
    const languagesList = document.getElementById('languages-list');

    if (buttonCloseLanguages.contains(event.target)) {
        return true;
    }

    if (!languagesList.contains(event.target) && languagesList.classList.contains('open')) {
        return true;
    }

    return false;
}

function isClickOnLang(event) {

    if (event.target.classList.contains('js-button-lang')) {
        return true;
    }

    return false;
}

function addBodyOverflow() {
    const main = document.getElementById('modal-main-content');

    fitElementToScrollPosition(main);

    document.body.classList.add('languages-list-open');

    document.documentElement.classList.add('languages-list-open');
}

function removeBodyOverflow() {
    const body = document.body;
    body.classList.remove('languages-list-open');
    const main = document.getElementById('modal-main-content');

    if (!body.classList.contains('languages-list-open') && !body.classList.contains('menu-mobile-open')) {
        removeScrollPositionFromElement(main);
    }

    document.documentElement.classList.remove('languages-list-open');
}

function openLanguagesList() {
    document.getElementById('languages-list').classList.add('open');
}

function closeLanguagesList() {
    document.getElementById('languages-list').classList.remove('open');
}

/**
 * Fit a scrollable element to the current scroll position.
 * 
 * Uses to prevent the main element to from "jumping" to the
 * top of the page.
 * 
 * @param {element} element HTML element to apply the operations
 */
function fitElementToScrollPosition(element) {        
    if (element.style.top) return;

    element.style.top = `-${window.scrollY}px`;
}

/**
 * Remove the scroll position fitted with the 
 * "fitElementToScrollPosition" function.
 * 
 * Uses to restore the initial state of the element.
 * 
 * @param {element} element HTML element to apply the operations
 */
function removeScrollPositionFromElement(element) {
    
    const scrollY = element.style.top;

    element.style.top = '';

    window.scrollTo(0, parseInt(scrollY) * -1);
}

function updateNavLang(event) {
    const target = event.target;

    const flagUrl = target.getElementsByClassName('js-lang-flag')[0].src;
    const langCode = target.dataset.lang;

    const mobileChangeLang = document.getElementById('mobile-button-lang');
    const mobileFlag = mobileChangeLang.getElementsByClassName('js-lang-flag')[0];
    const mobileLangCode = mobileChangeLang.getElementsByClassName('js-lang-code')[0];

    const buttonChangeLang = document.getElementById('nav-button-lang');
    const navFlag = buttonChangeLang.getElementsByClassName('js-lang-flag')[0];
    const navLangCode = buttonChangeLang.getElementsByClassName('js-lang-code')[0];

    navFlag.src = flagUrl;
    navLangCode.textContent = langCode;

    mobileFlag.src = flagUrl;
    mobileLangCode.textContent = langCode;

    return langCode;
}

async function postNewLang(newLang) {
    try {
        const data = {
            lang: newLang
        };

        const response = await axios.post('/', data);

        if (response.status !== 200) return;

        window.location.href = `${window.location.origin}/`;

    } catch (err) {
        console.error(err);
    }
}